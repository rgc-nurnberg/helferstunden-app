import React, { useState, useEffect } from 'react';
import Layout from '../Shared/Layout';
import { Calendar as BigCalendar, momentLocalizer } from 'react-big-calendar';
import moment from 'moment';
import 'moment/locale/de';
import 'moment/locale/en-gb';
import 'moment/locale/ru';


moment.updateLocale('de', {
    week: {
        dow: 1,
        doy: 4,
    },
});
moment.updateLocale('en-gb', {
    week: {
        dow: 1,
        doy: 4,
    },
});
moment.updateLocale('ru', {
    week: {
        dow: 1,
        doy: 4,
    },
});

const messages: any = {
    de: {
        date: 'Datum',
        time: 'Uhrzeit',
        event: 'Veranstaltung',
        allDay: 'Ganztägig',
        week: 'Woche',
        work_week: 'Arbeitswoche',
        day: 'Tag',
        month: 'Monat',
        previous: '<',
        next: '>',
        yesterday: 'Gestern',
        tomorrow: 'Morgen',
        today: 'Heute',
        agenda: 'Agenda',

        noEventsInRange: 'In diesem Intervall gibt es keine Veranstaltungen.',

        showMore: (total: any) => `+${total} weitere`
    }
};

const Calendar = () => {
    const myEventsList: any[] = [];
    const [language, setLanguage] = useState('de');
    const [localizer, setLocalizer] = useState(momentLocalizer(moment));

    useEffect(() => {
        moment.locale(language);
        setLocalizer(momentLocalizer(moment));
    }, [language]);

    return (
        <BigCalendar
            localizer={localizer}
            events={myEventsList}
            startAccessor="start"
            endAccessor="end"
            style={{ height: 500 }}
            messages={messages[language]}
        />
    );
};

Calendar.layout = (page: any) => {
    return <Layout title="Kalender">{page}</Layout>;
};

export default Calendar;

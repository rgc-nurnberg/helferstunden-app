import React from 'react';
import Layout from '../Shared/Layout';

const Dashboard = () => {
    return (
        <>
            <h1>Welcome</h1>
            <p>Hello, welcome to your first Inertia app!</p>
        </>
    );
};

Dashboard.layout = (page: any) => {
    return <Layout title="Dashboard">{page}</Layout>;
};

export default Dashboard;

import React, { useState, useContext, useRef } from 'react';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Alert from '@material-ui/lab/Alert';
import CircularProgress from '@material-ui/core/CircularProgress';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { deDE } from '@material-ui/core/locale';

import { Inertia } from '@inertiajs/inertia';
import { usePage, InertiaLink } from '@inertiajs/inertia-react';

import Copyright from '../Shared/Copyright';
import AppContext from '../Shared/AppContext';


declare function route(name: string): string;


const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    submitButtonProgress: {
        marginRight: 5,
    }
}));

export default function Login() {
    const classes = useStyles();

    const { setLoading } = useContext(AppContext);

    const emailRef = useRef<any>(null);
    const passwordRef = useRef<any>(null);
    const rememberRef = useRef<any>(null);

    const [validatingLogin, setValidatingLogin] = useState(false);

    // Errors
    const { errorBags } = usePage().props as any;
    const [formErrors, setFormErrors] = useState({
        email: undefined,
        password: undefined
    });

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        setValidatingLogin(true);

        const data = {
            email: emailRef.current === null ? '' : emailRef.current.value,
            password: passwordRef.current === null ? '' : passwordRef.current.value,
            remember: rememberRef.current === null ? false : rememberRef.current.checked
        };

        Inertia.post('/login', data, {
            onSuccess: () => {
                setFormErrors({
                    email: undefined,
                    password: undefined
                });

                setValidatingLogin(false);
            }
        });
    };

    const handleFormError = (fieldId: string, error: string) => {
        setFormErrors(errors => ({
            ...errors,
            [fieldId]: error,
        }));
    };

    return (
        <ThemeProvider theme={(outerTheme) => createMuiTheme(outerTheme, deDE)}>
            <Container component="main" maxWidth="xs">
                <CssBaseline />
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Anmelden
                    </Typography>
                    <form onSubmit={handleSubmit} className={classes.form}>
                        {!Object.values(formErrors).some(e => e) // Don't show previous login request errors if the form isn't valid with frontend validation
                            && errorBags.default && errorBags.default.login
                            && <Alert severity="error">{errorBags.default.login}</Alert>}

                        <TextField
                            inputRef={emailRef}
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            disabled={validatingLogin}
                            id="email"
                            label="E-Mail"
                            name="email"
                            autoComplete="email"
                            autoFocus
                            error={((errorBags.default && errorBags.default.email) || formErrors.email) && true}
                            helperText={formErrors.email ? formErrors.email : (errorBags.default && errorBags.default.email ? errorBags.default.email : '')}
                            onInvalid={(event: React.FormEvent<HTMLDivElement>) => { event.preventDefault(); handleFormError('email', 'Die E-Mail kann nicht leer sein'); }}
                        />

                        <TextField
                            inputRef={passwordRef}
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            disabled={validatingLogin}
                            name="password"
                            label="Passwort"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            error={((errorBags.default && errorBags.default.password) || formErrors.password) && true}
                            helperText={formErrors.password ? formErrors.password : (errorBags.default && errorBags.default.password ? errorBags.default.password : '')}
                            onInvalid={(event: React.FormEvent<HTMLDivElement>) => { event.preventDefault(); handleFormError('password', 'Das Passwort kann nicht leer sein'); }}
                        />

                        <FormControlLabel
                            control={<Checkbox inputRef={rememberRef} disabled={validatingLogin} id="remember" value="remember" color="primary" />}
                            label="Angemeldet bleiben"
                        />

                        <Button
                            type="submit"
                            fullWidth
                            disabled={validatingLogin}
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            {validatingLogin && <CircularProgress size={20} className={classes.submitButtonProgress} />}
                            Anmelden
                        </Button>

                        <Grid container>
                            <Grid item xs>
                                <Link component={InertiaLink}
                                    href="#"
                                    onClick={() => setLoading(true)}
                                    onFinish={() => setLoading(false)}
                                >
                                    Passwort vergessen?
                            </Link>
                            </Grid>
                            <Grid item>
                                <Link component={InertiaLink}
                                    href={route('login')}
                                    onClick={() => setLoading(true)}
                                    onFinish={() => setLoading(false)}
                                >
                                    Konto erstellen
                            </Link>
                            </Grid>
                        </Grid>
                    </form>
                </div>
                <Box mt={8}>
                    <Copyright />
                </Box>
            </Container>
        </ThemeProvider>
    );
}

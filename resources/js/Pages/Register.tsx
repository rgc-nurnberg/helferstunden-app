import React, { useRef, useState } from 'react';

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';
import Alert from '@material-ui/lab/Alert';

import { Inertia } from '@inertiajs/inertia';
import { InertiaLink, usePage } from '@inertiajs/inertia-react';

import Copyright from '../Shared/Copyright';


declare function route(name: string): string;


const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    submitButtonProgress: {
        marginRight: 5,
    },
}));

export default function Register() {
    const classes = useStyles();

    const firstnameRef = useRef<any>(null);
    const lastnameRef = useRef<any>(null);
    const emailRef = useRef<any>(null);
    const passwordRef = useRef<any>(null);
    const passwordConfirmationRef = useRef<any>(null);

    const [validatingRegister, setValidatingRegister] = useState(false);

    const { errorBags } = usePage().props as any;
    const [formErrors, setFormErrors] = useState({
        firstname: undefined,
        lastname: undefined,
        email: undefined,
        password: undefined,
        passwordConfirmation: undefined
    });

    const formFieldHasError = (fieldname: 'firstname' | 'lastname' | 'email' | 'password' | 'passwordConfirmation') => {
        return ((errorBags.default && errorBags.default[fieldname]) || formErrors[fieldname]) && true;
    };

    const formFieldHelperText = (fieldname: 'firstname' | 'lastname' | 'email' | 'password' | 'passwordConfirmation') => {
        return formErrors[fieldname] ? formErrors[fieldname] : (errorBags.default && errorBags.default[fieldname] ? errorBags.default[fieldname] : '');
    };

    const handleFormError = (fieldId: string, error: string) => {
        setFormErrors(errors => ({
            ...errors,
            [fieldId]: error,
        }));
    };

    const onFormFieldEmpty = (event: React.FormEvent<any>, fieldname: string, label: string) => {
        event.preventDefault();
        handleFormError(fieldname, `${label} kann nicht leer sein`);
    };

    const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        setValidatingRegister(true);

        const data = {
            firstname: firstnameRef.current === null ? '' : firstnameRef.current.value,
            lastname: lastnameRef.current === null ? '' : lastnameRef.current.value,
            email: emailRef.current === null ? '' : emailRef.current.value,
            password: passwordRef.current === null ? '' : passwordRef.current.value,
            password_confirmation: passwordConfirmationRef.current === null ? '' : passwordConfirmationRef.current.value,
        };

        Inertia.post('/register', data)
            .then(response => {
                setFormErrors({
                    firstname: undefined,
                    lastname: undefined,
                    email: undefined,
                    password: undefined,
                    passwordConfirmation: undefined
                });

                setValidatingRegister(false);

                return response;
            });
    };

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Registrieren
                </Typography>
                <form onSubmit={handleSubmit} className={classes.form}>
                    {!Object.values(formErrors).some(e => e) // Don't show previous login request errors if the form isn't valid with frontend validation
                        && errorBags.default && errorBags.default.register
                        && <Alert severity="error">{errorBags.default.register}</Alert>}

                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                inputRef={firstnameRef}
                                name="firstname"
                                autoComplete="fname"
                                variant="outlined"
                                required
                                fullWidth
                                disabled={validatingRegister}
                                id="firstname"
                                label="Vorname"
                                autoFocus
                                error={formFieldHasError('firstname')}
                                helperText={formFieldHelperText('firstname')}
                                onInvalid={(event: React.FormEvent<any>) => onFormFieldEmpty(event, 'firstname', 'Vorname')}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                inputRef={lastnameRef}
                                name="lastname"
                                autoComplete="lname"
                                variant="outlined"
                                required
                                fullWidth
                                disabled={validatingRegister}
                                id="lastname"
                                label="Nachname"
                                error={formFieldHasError('lastname')}
                                helperText={formFieldHelperText('lastname')}
                                onInvalid={(event: React.FormEvent<any>) => onFormFieldEmpty(event, 'lastname', 'Nachname')}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                inputRef={emailRef}
                                name="email"
                                autoComplete="email"
                                variant="outlined"
                                required
                                fullWidth
                                disabled={validatingRegister}
                                id="email"
                                label="E-Mail"
                                error={formFieldHasError('email')}
                                helperText={formFieldHelperText('email')}
                                onInvalid={(event: React.FormEvent<any>) => onFormFieldEmpty(event, 'email', 'E-Mail')}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                inputRef={passwordRef}
                                name="password"
                                variant="outlined"
                                required
                                fullWidth
                                disabled={validatingRegister}
                                label="Passwort"
                                type="password"
                                id="password"
                                error={formFieldHasError('password')}
                                helperText={formFieldHelperText('password')}
                                onInvalid={(event: React.FormEvent<any>) => onFormFieldEmpty(event, 'password', 'Passwort')}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                inputRef={passwordConfirmationRef}
                                name="passwordConfirmation"
                                variant="outlined"
                                required
                                fullWidth
                                disabled={validatingRegister}
                                label="Passwort"
                                type="password"
                                id="passwordConfirmation"
                                error={formFieldHasError('passwordConfirmation')}
                                helperText={formFieldHelperText('passwordConfirmation')}
                                onInvalid={(event: React.FormEvent<any>) => onFormFieldEmpty(event, 'passwordConfirmation', 'Passwortbestätigung')}
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        disabled={validatingRegister}
                    >
                        {validatingRegister && <CircularProgress size={20} className={classes.submitButtonProgress} />}
                        Registrieren
                    </Button>
                    <Grid container justify="flex-end">
                        <Grid item>
                            <InertiaLink href={route('login')} className="MuiTypography-root MuiLink-root MuiLink-underlineHover MuiTypography-colorPrimary">
                                Du hast bereits ein Konto? Anmelden
                            </InertiaLink>
                        </Grid>
                    </Grid>
                </form>
            </div>
            <Box mt={5}>
                <Copyright />
            </Box>
        </Container>
    );
}

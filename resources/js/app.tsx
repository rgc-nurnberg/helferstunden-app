import React, { useState } from 'react';
import { render } from 'react-dom';
import { InertiaApp } from '@inertiajs/inertia-react';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import AppContext from './Shared/AppContext';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { deDE } from '@material-ui/core/locale';


const theme = createMuiTheme({}, deDE);

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        backdrop: {
            zIndex: theme.zIndex.drawer + 1,
            color: '#fff',
        },
    }),
);

function App() {
    const classes = useStyles();
    const [loading, setLoading] = useState(false);

    return (
        <ThemeProvider theme={theme}>
            <AppContext.Provider value={{ setLoading }}>
                <InertiaApp
                    initialPage={app ? JSON.parse(app.dataset.page!) : '{}'}
                    resolveComponent={name =>
                        import(`./Pages/${name}`)
                            .then(({ default: page }) => page)
                    }
                />
                <Backdrop className={classes.backdrop} open={loading}>
                    <CircularProgress color="inherit" />
                </Backdrop>
            </AppContext.Provider>
        </ThemeProvider>
    );
}


const app = document.getElementById('app');

render(
    <App />,
    app
);

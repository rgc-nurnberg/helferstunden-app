import React from 'react';
import Link from '@material-ui/core/Link';
import ListItem from '@material-ui/core/ListItem';
import { InertiaLink } from '@inertiajs/inertia-react';


function ListItemLink({
    children,
    href,
    itemKey,
    selected = false,
    only = [],
    onClick,
    onStart,
    onFinish
}: {
    children: any;
    href: string;
    itemKey: string;
    selected?: boolean;
    only?: string[];
    onClick?: (event: any) => void;
    onStart?: () => void;
    onFinish?: () => void;
}) {
    return (
        <Link component={InertiaLink} href={href} color='inherit' underline='none' only={only} onClick={onClick} onStart={onStart} onFinish={onFinish}>
            <ListItem button key={itemKey} selected={selected}>
                {children}
            </ListItem>
        </Link>
    );
}

export default React.memo(ListItemLink);

<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::inertia('/', 'Dashboard')->middleware('auth');
Route::inertia('/dashboard', 'Dashboard')->name('dashboard')->middleware('auth');
Route::inertia('/calendar', 'Calendar')->name('calendar')->middleware('auth');
